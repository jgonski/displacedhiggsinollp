for m in 115 150 200 250 300 350 400 450 500 600 700 800 ; do
    for t in 2 10 20 50; do
      for final_state in HyyHSM HyyZSM ZeeHSM ZeeZSM; do

      if [[ "$final_state" == *"H"* ]] && [[ $m -lt 125 ]]; then
        continue
      fi

      if [[ "$final_state" == *"Hyy"* ]]; then
        if [[ $m -gt 500 ]]; then
          continue
        elif [[ $t -gt 25 ]]; then
          continue
        elif [[ $m -gt 300 ]] && [[ $t -gt 10 ]]; then
          continue
        fi
      fi

      if [[ "$final_state" == *"Zee"* ]]; then
        if [[ $m -gt 600 ]] && [[ $t -gt 10 ]]; then
          continue
        elif [[ $m -gt 300 ]] && [[ $t -gt 25 ]]; then
          continue
        elif [[ $m -gt 200 ]] && [[ $(( $m % 100 )) -gt 0 ]]; then # here, gt --> not equal to
          continue
        fi
      fi
      #echo $m $t $final_state
      cp template.mc.MGPy8EG_A14N23LO_GGMHinohyy_MASS_LIFETIME_FINALSTATE.py mc.MGPy8EG_A14N23LO_GGMHinohyy_${m}_${t}ns_${final_state}.py
    done     
  done
done

