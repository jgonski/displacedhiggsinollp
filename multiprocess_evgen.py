import os
import sys
import time
import math
import numpy as np
import multiprocessing as mp

NCORES = mp.cpu_count()
NCORES_MAX = 10 #NCORES - 6
print "NCORES =", NCORES
print "RUNNING ON NCORES =", NCORES_MAX

cwd = os.getcwd()

infile_path = "/data/users/kkennedy/MCPrivateProduction/Run/11_13_SignalGrid/displacedhiggsinollp/"

def generate(tag):

  output_dir = "LocalRun_Hino_"+tag

  os.mkdir(output_dir)
  os.chdir(output_dir)

  os.mkdir("999999")
  os.system( "cp "+infile_path+"MadGraphControl_SimplifiedModel_C1N2N1_GGMHino_Filter.py 999999" )
  os.system( "cp "+infile_path+"mc.MGPy8EG_A14N23LO_GGMHinohyy_"+tag+".py 999999" )
  os.system( "cp /data/users/kkennedy/MCPrivateProduction/xAOD/MakeTree.py . " )

  command_gen = " asetup AthGeneration,21.6.42 ; Gen_tf.py --ecmEnergy 13000 --firstEvent 1 --maxEvents 500 --randomSeed 10041992 --jobConfig 999999 --outputEVNTFile EVNT.root"

  command_daod = "asetup 21.2.86.0,AthDerivation ; Reco_tf.py --inputEVNTFile EVNT.root --outputDAODFile Hino.root --reductionConf TRUTH1 "

  command_maketree = "asetup AnalysisBase,21.2.42,here ; python MakeTree.py False True truthNTuple DAOD_TRUTH1.Hino.root "

  os.system( command_gen )
  os.system( command_daod )
  os.system( command_maketree )

  os.chdir(cwd)

# -------------------------------------------------------------------------------------------------
def main():

  arglist_full = []

  for m in [100, 150, 200, 300, 400, 500, 600, 800, 1000]:
    for t in [2, 10]:
      for final_state in ["HyyHSM", "HyyZSM", "ZeeHSM", "ZeeZSM"]:

        if "H" in final_state and m < 125: continue
        if "Hyy" in final_state and m > 600: continue
      
	arglist_full.append( str(m)+"_"+str(t)+"ns_"+final_state )

  Nargs = len(arglist_full)
  NProcesses = NCORES_MAX
  if arglist_full < NCORES_MAX: NProcesses = len(arglist_full)

  arglist = []
  for i in range(Nargs):

    if i > 0 and i % NProcesses == 0: 
      pool = mp.Pool(processes=NProcesses)
      pool.map(generate, arglist )
      pool.terminate()
      arglist = []

    arglist.append( arglist_full[i] )

# -------------------------------------------------------------------------------------------------
if __name__ == '__main__':
  main()
